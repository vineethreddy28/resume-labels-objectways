import csv
import json
import os


def read_json_file(file_name):
    file_path = '\\'.join([os.getcwd(), file_name])
    data = []
    with open(file_path, 'r') as f:
        json_data = json.load(f)
        for j_ent in json_data:
            data.append(j_ent)
    return data


def create_entity_csv_file(read_json_filename, response_csv_filename):
    try:
        resume_lebels_csv_filepath = '\\'.join([os.getcwd(), response_csv_filename])
        with open(resume_lebels_csv_filepath, 'w', newline='') as csvfile:
            fieldnames = ['File Name', 'Entity_Type', 'Counts of Entities']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()

            data = read_json_file(read_json_filename)
            for r_data in data:
                obj_data = dict()

                def add_func(i):
                    for an_data in i['annotations']['tags']:
                        if an_data['type'] not in obj_data:
                            obj_data[an_data['type']] = 1
                        else:
                            obj_data[an_data['type']] += 1

                if 'annotations' in r_data:
                    add_func(r_data)

                # If we need to add annotations_trail data
                # if 'annotations_trail' in r_data:
                #     for ann_trail_data in r_data['annotations_trail']:
                #         if 'annotations' in ann_trail_data:
                #             add_func(ann_trail_data)
                for obj_key in obj_data:
                    writer.writerow({'File Name': r_data['file_name'],
                                     'Entity_Type': obj_key,
                                     'Counts of Entities': obj_data[obj_key]})
        print(resume_lebels_csv_filepath)
        return {'csv_file_path': resume_lebels_csv_filepath}
    except Exception as err:
        return f"Error while creating resume labels csv file, ERROR: {err}"


# Generate CSV file in project location
# json_filename = 'resume_labels.json'
# csv_filename = 'resume_labels.csv'
# create_entity_csv_file(json_filename, csv_filename)
