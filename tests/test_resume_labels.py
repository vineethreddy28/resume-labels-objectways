import os

import pytest
import uvicorn
import requests
import subprocess
import time
import atexit

#  Server startup time. There's better ways to do this, but have some issues so staring server manually.
# # app_file = '\\'.join([os.getcwd(), 'app.py'])
# server = subprocess.Popen(['python', 'app.py'])
#
# time.sleep(1) #

#
# def close_server():
#     server.terminate()
#     server.wait(10)
#
#
# atexit.register(close_server)


def test_get_entities():
    """
    Testing get entities GET API with inputs json_file_name=resume_labels.json and file_name=Aditya Meherish.pdf
    """
    r = requests.get('http://localhost:81/get-entities?file_name=Aditya%20Meherish.pdf&json_file_name=resume_labels.json')
    assert r.status_code == 200, r.text
    assert r.headers['content-type'].startswith('application/json')
    assert r.json() == [
        {
            "Position": 2
        },
        {
            "Achievement": 19
        },
        {
            "Qualification": 1
        },
        {
            "General Skills": 12
        }
    ]


def test_transform_json_to_csv():
    """
    Test transform-json-to-csv API for input json_file_name=resume_labels.json
    """
    r = requests.get('http://localhost:81/transform-json-to-csv/?json_file_name=resume_labels.json')
    assert r.status_code == 200, r.text
    assert r.headers['content-type'].startswith('application/json')
    assert r.json()['csv_file_path'] == "C:\\Users\\Public\\Documents\\python_resume_labels\\resume_labels.csv"
