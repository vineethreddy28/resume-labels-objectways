import csv
import json
import os

import uvicorn
from fastapi import FastAPI

from resume_labels_funcs import read_json_file, create_entity_csv_file

dir = os.path.dirname(__file__)

app = FastAPI()


@app.get('/get-entities')
async def get_entities_information(json_file_name: str, file_name: str):
    """
    Take json_file_name and file_name as input in params
    :param json_file_name: JSON file name to read data
    :param file_name: As string values from JSON content from each line of “resume_labels.json”
           Passing from external client, we can take from json file as well for all available json line in .json file.
    :return: List of all “types” and number of occurrences”  e.g. [ { “Technical Skills” : 5 }, .... ]
    """
    final_response = []

    json_data = read_json_file(json_file_name)

    for j_ent in json_data:
        if j_ent['file_name'] == file_name:
            obj_data = dict()

            def add_func(i):
                for an_data in i['annotations']['tags']:
                    if an_data['type'] not in obj_data:
                        obj_data[an_data['type']] = 1
                    else:
                        obj_data[an_data['type']] += 1

            if 'annotations' in j_ent:
                add_func(j_ent)

            # If we need to add annotations_trail data
            # if 'annotations_trail' in r_data:
            #     for ann_trail_data in r_data['annotations_trail']:
            #         if 'annotations' in ann_trail_data:
            #             add_func(ann_trail_data)

            for obj_key in obj_data:
                final_response.append({obj_key: obj_data[obj_key]})

    return final_response


@app.get("/transform-json-to-csv")
async def create_upload_file(json_file_name: str):
    """
    Take Json file_name as input in params
    :param json_file_name:
    :param file: Taking JSON file as input
    :return: converting CSV as output
    """
    try:
        # Opening JSON file and loading the data
        # into the variable data w.r.t windows OS filepath
        # with open('\\'.join([os.getcwd(), json_file_name])) as json_file:
        #     data = json.load(json_file)
        #     print(data)

        json_filename = 'resume_labels.json'
        csv_filename = 'resume_labels.csv'
        response = create_entity_csv_file(json_filename, csv_filename)
        return response

    except Exception as e:
        return str(e)


if __name__ == '__main__':
    uvicorn.run(app, host='localhost', port=81)
